#Dependency python-serial
import serial
import time

def transmitbinary(binarynumber):
    port =serial.Serial(

    port='/dev/ttyACM0',
    baudrate=9600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout=1.8
    )

    print("start")
    port.flush()
    #clear buffer
    #port.write(("RESET").encode())
    ready = port.read(29)
    print(ready)
    sendto = "2"
    port.write(sendto.encode()) #set mode 2
    time.sleep(1)
    #rcv1 = port.readline()
    #print(rcv1)
    #if rcv1 == b'SENDMODE\r\n' :
    #    print("Sendmode recieved")
    port.write((binarynumber).encode())
    rcv3 = port.readline()
    print(rcv3)
    if rcv3 == b'TRANSMITTING\r\n':
        print("success")
        port.flush()

def receivebinary():
    port =serial.Serial(

    port='/dev/ttyACM0',
    baudrate=9600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout=3.0
    )

    print("start")

    #clear buffer
    port.write(("RESET").encode())
    ready = port.read(50)
    print(ready)

    sendto = "3"
    port.write(sendto.encode()) #set mode 2
    time.sleep(1)
    rcv1 = port.readline()
    print(rcv1)
    if rcv1 == b'RECEIVEMODE\r\n' :
        print("RECEIVEMODE recieved")
        port.write(("2").encode())
        rcv2 = port.readline()
        rcv3 = port.readline()
        print(rcv2)
        print("pharsed")
        binaryrecieved = rcv2.decode("utf-8")
        binaryrecieved = binaryrecieved.replace("\n","")
        print(binaryrecieved)
        return binaryrecieved
