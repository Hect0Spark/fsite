from flask import Flask, request, redirect, url_for
from datetime import datetime, date, timedelta
from shhFunctions import UART_functions
from flask_sqlalchemy import SQLAlchemy

import time
import os
import serial

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] ='sqlite:////home/pi/FSite/SHH.db'
db = SQLAlchemy(app)

#db stuff
class DeviceTable(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True, nullable=False)
    modes = db.Column(db.String(120), unique=False, nullable=False)
    MN1 = db.Column(db.String(120), unique=False, nullable=True)
    MN2 = db.Column(db.String(120), unique=False, nullable=True)
    MN3 = db.Column(db.String(120), unique=False, nullable=True)
    MN4 = db.Column(db.String(120), unique=False, nullable=True)
    MN5 = db.Column(db.String(120), unique=False, nullable=True)
    MN6 = db.Column(db.String(120), unique=False, nullable=True)
    MN7 = db.Column(db.String(120), unique=False, nullable=True)
    MN8 = db.Column(db.String(120), unique=False, nullable=True)
    MN9 = db.Column(db.String(120), unique=False, nullable=True)
    MN10 = db.Column(db.String(120), unique=False, nullable=True)
    B1 = db.Column(db.String(120), unique=False, nullable=True)
    B2 = db.Column(db.String(120), unique=False, nullable=True)
    B3 = db.Column(db.String(120), unique=False, nullable=True)
    B4 = db.Column(db.String(120), unique=False, nullable=True)
    B5 = db.Column(db.String(120), unique=False, nullable=True)
    B6 = db.Column(db.String(120), unique=False, nullable=True)
    B7 = db.Column(db.String(120), unique=False, nullable=True)
    B8 = db.Column(db.String(120), unique=False, nullable=True)
    B9 = db.Column(db.String(120), unique=False, nullable=True)
    B10 = db.Column(db.String(120), unique=False, nullable=True)

class Settings(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    settingname = db.Column(db.String(120), unique=True, nullable=False)
    settingvalue = db.Column(db.String(120), unique=True, nullable=False)

RemoteServerIP = "192.168.1.2/MobileWebsite/Project%20Mobile%20Website"
RemoteServerTest = "False"
fmt = '%Y-%m-%d %H:%M:%S'
#Login Globals
loggedin = "false"
logintime = datetime.now()
loggedinclientip = "0"
ipglobal = ""

@app.route("/")

def index():
    return

@app.route("/logincheck", methods=['GET', 'POST'])

def logincheck():
    username = request.form.get('username')
    password = request.form.get('password')
    global loggedinclientip
    loggedinclientip = str(request.environ.get('HTTP_X_REAL_IP', request.remote_addr))
    global logintime
    logintime = datetime.now()
    global loggedin
    loggedin = "true"
    if RemoteServerTest == "True":
        IP = RemoteServerIP
    else:
        IP = request.args['IP']
    success = "false"

    if Settings.query.filter_by(settingname = "user").first():
        account = Settings.query.filter_by(settingname = "user").first()
        usernamedb = account.settingvalue
        passw = Settings.query.filter_by(settingname = "password").first()
        passworddb = passw.settingvalue
    else:
        usernameadd = Settings(settingname = "user", settingvalue = "admin")
        db.session.add(usernameadd)
        db.session.commit()
        passwordadd = Settings(settingname = "password", settingvalue = "password")
        db.session.add(passwordadd)
        db.session.commit()
        SecretKeyadd = Settings(settingname = "IFTTTSecretKey", settingvalue = "0770431680229860")
        db.session.add(SecretKeyadd)
        db.session.commit()
        Enabledadd = Settings(settingname = "IFTTTEnabled", settingvalue = "False")
        db.session.add(Enabledadd)
        db.session.commit()
        usernamedb = "admin"
        passworddb = "password"

    if username == usernamedb and password == passworddb:
        print("LOGIN admin SUCCESS at " + str(logintime) + " by " + str(loggedinclientip))
        success = "true"

    return redirect("http://" + IP + "/?loginsuccess=" + success + "&username=" + username)


@app.route("/checkLoginTime")
def checkLoginTime():
    global loggedin
    global logintime
    if(loggedin == "true"):
        timenow = datetime.now()
        minutediff=getTimeDifferenceFromNow(logintime, timenow)
        print(str(minutediff))
        if(minutediff > 15):
            loggedin = false
            return "Expired"
        else:
            return "Logged in"
    else:
        return "not logged in"

@app.route("/Logout", methods=['GET'])
def Logout():
    global loggedin
    global loggedinclientip
    loggedinclientip1 = str(request.environ.get('HTTP_X_REAL_IP', request.remote_addr))
    if RemoteServerTest == "True":
        IP = RemoteServerIP
    else:
        IP = request.args['IP']

    if(loggedinclientip == loggedinclientip1 and loggedin == "true"):
        loggedin = "false"
        print("Logged Out")
        return redirect("http://" + IP + "/?page=logout2")
    else:
        print("Attempted hijack logout")
        return "error"

@app.route("/RebootPi",methods=['POST'])
def RebootPi():
    global loggedin
    global loggedinclientip
    loggedinclientip1 = str(request.environ.get('HTTP_X_REAL_IP', request.remote_addr))
    print(loggedinclientip)
    print(loggedinclientip1)
    print(checkLoginTime)
    print(loggedin)
    if(loggedinclientip == loggedinclientip1 and loggedin == "true"):
        os.system("sudo reboot")
    else:
        return "error you are not logged into SHH"

@app.route("/IFTTT",methods=['GET', 'POST'])
def IFTTT():
    IFTTTSecret = Settings.query.filter_by(settingname = "IFTTTSecretKey").first()
    IFTTTSecretKey= IFTTTSecret.settingvalue
    IFTTTEn = Settings.query.filter_by(settingname = "IFTTTEnabled").first()
    IFTENABLED = IFTTTEn.settingvalue
    apikey = IFTTTSecretKey
    security = request.args['apikey']
    deviceid = request.args['deviceid']
    mode = request.args['mode']
    if(apikey == security and IFTENABLED == "True"):
        print("IFTTT accepted")
        print(deviceid)
        print(mode)
        Loadtopair = DeviceTable.query.filter_by(name = deviceid).first()
        WMN1 = Loadtopair.MN1
        WMN2 = Loadtopair.MN2
        WMN3 = Loadtopair.MN3
        WMN4 = Loadtopair.MN4
        WMN5 = Loadtopair.MN5
        WMN6 = Loadtopair.MN6
        WMN7 = Loadtopair.MN7
        WMN8 = Loadtopair.MN8
        WMN9 = Loadtopair.MN9
        WMN10 = Loadtopair.MN10
        WB1 = Loadtopair.B1
        WB2 = Loadtopair.B2
        WB3 = Loadtopair.B3
        WB4 = Loadtopair.B4
        WB5 = Loadtopair.B5
        WB6 = Loadtopair.B6
        WB7 = Loadtopair.B7
        WB8 = Loadtopair.B8
        WB9 = Loadtopair.B9
        WB10 = Loadtopair.B10
        if(mode == "1"):
            UART_functions.transmitbinary(WB1)
        if(mode == "2"):
            UART_functions.transmitbinary(WB2)
        if(mode == "3"):
            UART_functions.transmitbinary(WB3)
        if(mode == "4"):
            UART_functions.transmitbinary(WB4)
        if(mode == "5"):
            UART_functions.transmitbinary(WB5)
        if(mode == "6"):
            UART_functions.transmitbinary(WB6)
        if(mode == "7"):
            UART_functions.transmitbinary(WB7)
        if(mode == "8"):
            UART_functions.transmitbinary(WB8)
        if(mode == "9"):
            UART_functions.transmitbinary(WB9)
        if(mode == "10"):
            UART_functions.transmitbinary(WB10)


    else:
        print(security)
        print("IFTTT denied, invalid apikey")
    return "IFTTT WEBHOOKS"

@app.route("/Receive",methods=['GET', 'POST'])
def Receive():
    #Receive all variables from POST
    Modeselect = request.form.get('Modeselect')
    Name1 = request.form.get('Name')
    WMN1 = request.form.get('MN1')
    WMN2 = request.form.get('MN2')
    WMN3 = request.form.get('MN3')
    WMN4 = request.form.get('MN4')
    WMN5 = request.form.get('MN5')
    WMN6 = request.form.get('MN6')
    WMN7 = request.form.get('MN7')
    WMN8 = request.form.get('MN8')
    WMN9 = request.form.get('MN9')
    WMN10 = request.form.get('MN10')
    WB1 = request.form.get('B1')
    WB2 = request.form.get('B2')
    WB3 = request.form.get('B3')
    WB4 = request.form.get('B4')
    WB5 = request.form.get('B5')
    WB6 = request.form.get('B6')
    WB7 = request.form.get('B7')
    WB8 = request.form.get('B8')
    WB9 = request.form.get('B9')
    WB10 = request.form.get('B10')
    pair = request.form.get('pair')

    #Check if on TEST
    if RemoteServerTest == "True":
        IP = RemoteServerIP
    else:
        IP = request.args['IP']

    if pair != "Save":
        if pair != "Delete":
            if pair != "Load":
                binaryrec = UART_functions.receivebinary()
                returnvalue = "Received: " + binaryrec
                if binaryrec == "READY":
                    binaryrec = "ERROR"
    global ipglobal
    ipglobal = IP
    print(pair)
    #pairing Check
    if pair == "1":
        WB1 = binaryrec

    elif pair == "2":
        WB2 = binaryrec

    elif pair == "3":
        WB3 = binaryrec

    elif pair == "4":
        WB4 = binaryrec

    elif pair == "5":
        WB5 = binaryrec

    elif pair == "6":
        WB6 = binaryrec

    elif pair == "7":
        WB7 = binaryrec

    elif pair == "8":
        WB8 = binaryrec

    elif pair == "9":
        WB9 = binaryrec

    elif pair == "10":
        WB10 = binaryrec

    elif pair == "Save":
        #Check if already exists
        if DeviceTable.query.filter_by(name = Name1).first():
            print("already exists")
            updatedevice = DeviceTable.query.filter_by(name = Name1).first()
            updatedevice.name = Name1
            updatedevice.modes = Modeselect
            updatedevice.MN1 = WMN1
            updatedevice.MN2 = WMN2
            updatedevice.MN3 = WMN3
            updatedevice.MN4 = WMN4
            updatedevice.MN5 = WMN5
            updatedevice.MN6 = WMN6
            updatedevice.MN7 = WMN7
            updatedevice.MN8 = WMN8
            updatedevice.MN9 = WMN9
            updatedevice.MN10 = WMN10
            updatedevice.B1 = WB1
            updatedevice.B2 = WB2
            updatedevice.B3 = WB3
            updatedevice.B4 = WB4
            updatedevice.B5 = WB5
            updatedevice.B6 = WB6
            updatedevice.B7 = WB7
            updatedevice.B8 = WB8
            updatedevice.B9 = WB9
            updatedevice.B10 = WB10
            db.session.commit()
        else:
            print("does not already exist")
            adddevice = DeviceTable(name=Name1, modes = Modeselect, MN1 = WMN1, MN2 = WMN2, MN3 = WMN3, MN4 = WMN4, MN5 = WMN5, MN6 = WMN6, MN7 = WMN7, MN8 = WMN8, MN9 = WMN9, MN10 = WMN10, B1 = WB1, B2 = WB2, B3 = WB3, B4 = WB4, B5 = WB5, B6 = WB6, B7 = WB7, B8 = WB8, B9 = WB9, B10 = WB10)
            db.session.add(adddevice)
            db.session.commit()
    #delete
    elif pair == "Delete":
        if DeviceTable.query.filter_by(name = Name1).first():
            print("Deleting")
            DeviceTable.query.filter_by(name = Name1).delete()
            db.session.commit()
            Name1 = ""
            Modeselect = ""
            WMN1 = ""
            WMN2 = ""
            WMN3 = ""
            WMN4 = ""
            WMN5 = ""
            WMN6 = ""
            WMN7 = ""
            WMN8 = ""
            WMN9 = ""
            WMN10 = ""
            WB1 = ""
            WB2 = ""
            WB3 = ""
            WB4 = ""
            WB5 = ""
            WB6 = ""
            WB7 = ""
            WB8 = ""
            WB9 = ""
            WB10 = ""

            #print(test.name)
    elif pair == "Load":
        return redirect(url_for('ReceiveLoadlist', IP = IP))
    return redirect("http://" + IP + "/?page=pairing" + "&Loadselect="  + "&Modeselect=" + Modeselect + "&Name=" + Name1 + "&MN1=" + WMN1 + "&MN2=" + WMN2 + "&MN3=" + WMN3 + "&MN4=" + WMN4 + "&MN5=" + WMN5 + "&MN6=" + WMN6 + "&MN7=" + WMN7 + "&MN8=" + WMN8 + "&MN9=" + WMN9 + "&MN10=" + WMN10 + "&B1=" + WB1 + "&B2=" + WB2 + "&B3=" + WB3 + "&B4=" + WB4 + "&B5=" + WB5 + "&B6=" + WB6 + "&B7=" + WB7 + "&B8=" + WB8 + "&B9=" + WB9 + "&B10=" + WB10)


@app.route("/ReceiveLoadlist/")
def ReceiveLoadlist():
    device_names = [] # i initialize a list
    all_devices = DeviceTable.query.all() #query all devices
    devicenameindex = 0
    getstring = "http://" + ipglobal + "/?page=loaddevicePairing"
    for device in all_devices:
        print(device.name)
        #device_names.append(device.name) #iterate through all the devices and add the name attribute to the list
        #devicenameindex = devicenameindex + 1
        getstring = getstring + "&devicelist[]=" + device.name
    print(getstring)
    return redirect(getstring)

@app.route("/ReceiveLoad",methods=['GET', 'POST'])
def ReceiveLoad():
    selected = request.form.get('selected')
    if RemoteServerTest == "True":
        IP = RemoteServerIP
    else:
        IP = request.args['IP']
    Loadtopair = DeviceTable.query.filter_by(name = selected).first()
    Name1 = selected
    Modeselect = Loadtopair.modes
    WMN1 = Loadtopair.MN1
    WMN2 = Loadtopair.MN2
    WMN3 = Loadtopair.MN3
    WMN4 = Loadtopair.MN4
    WMN5 = Loadtopair.MN5
    WMN6 = Loadtopair.MN6
    WMN7 = Loadtopair.MN7
    WMN8 = Loadtopair.MN8
    WMN9 = Loadtopair.MN9
    WMN10 = Loadtopair.MN10
    WB1 = Loadtopair.B1
    WB2 = Loadtopair.B2
    WB3 = Loadtopair.B3
    WB4 = Loadtopair.B4
    WB5 = Loadtopair.B5
    WB6 = Loadtopair.B6
    WB7 = Loadtopair.B7
    WB8 = Loadtopair.B8
    WB9 = Loadtopair.B9
    WB10 = Loadtopair.B10
    return redirect("http://" + IP + "/?page=pairing" + "&Loadselect" + "&Modeselect=" + Modeselect + "&Name=" + Name1 + "&MN1=" + WMN1 + "&MN2=" + WMN2 + "&MN3=" + WMN3 + "&MN4=" + WMN4 + "&MN5=" + WMN5 + "&MN6=" + WMN6 + "&MN7=" + WMN7 + "&MN8=" + WMN8 + "&MN9=" + WMN9 + "&MN10=" + WMN10 + "&B1=" + WB1 + "&B2=" + WB2 + "&B3=" + WB3 + "&B4=" + WB4 + "&B5=" + WB5 + "&B6=" + WB6 + "&B7=" + WB7 + "&B8=" + WB8 + "&B9=" + WB9 + "&B10=" + WB10)
#remoteload

@app.route("/RemoteLoadlist",methods=['GET', 'POST'])
def RemoteLoadlist():
    if RemoteServerTest == "True":
        IP = RemoteServerIP
    else:
        IP = request.args['IP']
    device_names = [] # i initialize a list
    all_devices = DeviceTable.query.all() #query all devices
    devicenameindex = 0
    getstring = "http://" + IP + "/?page=loadremote"
    for device in all_devices:
        print(device.name)
        #device_names.append(device.name) #iterate through all the devices and add the name attribute to the list
        #devicenameindex = devicenameindex + 1
        getstring = getstring + "&devicelist[]=" + device.name
    print(getstring)
    return redirect(getstring)


@app.route("/RemoteLoad",methods=['GET', 'POST'])
def RemoteLoad():
    selected = request.form.get('selected')
    if RemoteServerTest == "True":
        IP = RemoteServerIP
    else:
        IP = request.args['IP']
    Loadtopair = DeviceTable.query.filter_by(name = selected).first()
    Name1 = selected
    Modeselect = Loadtopair.modes
    WMN1 = Loadtopair.MN1
    WMN2 = Loadtopair.MN2
    WMN3 = Loadtopair.MN3
    WMN4 = Loadtopair.MN4
    WMN5 = Loadtopair.MN5
    WMN6 = Loadtopair.MN6
    WMN7 = Loadtopair.MN7
    WMN8 = Loadtopair.MN8
    WMN9 = Loadtopair.MN9
    WMN10 = Loadtopair.MN10
    WB1 = Loadtopair.B1
    WB2 = Loadtopair.B2
    WB3 = Loadtopair.B3
    WB4 = Loadtopair.B4
    WB5 = Loadtopair.B5
    WB6 = Loadtopair.B6
    WB7 = Loadtopair.B7
    WB8 = Loadtopair.B8
    WB9 = Loadtopair.B9
    WB10 = Loadtopair.B10
    return redirect("http://" + IP + "/?page=remote" + "&Loadselect" + "&Modeselect=" + Modeselect + "&Name=" + Name1 + "&MN1=" + WMN1 + "&MN2=" + WMN2 + "&MN3=" + WMN3 + "&MN4=" + WMN4 + "&MN5=" + WMN5 + "&MN6=" + WMN6 + "&MN7=" + WMN7 + "&MN8=" + WMN8 + "&MN9=" + WMN9 + "&MN10=" + WMN10 + "&B1=" + WB1 + "&B2=" + WB2 + "&B3=" + WB3 + "&B4=" + WB4 + "&B5=" + WB5 + "&B6=" + WB6 + "&B7=" + WB7 + "&B8=" + WB8 + "&B9=" + WB9 + "&B10=" + WB10)

@app.route("/Transmit",methods=['GET', 'POST'])
def Transmit():
    if RemoteServerTest == "True":
        IP = RemoteServerIP
    else:
        IP = request.args['IP']
    ToggleNumber = request.form.get('Toggle')
    Modeselect = request.form.get('Modeselect')
    Name1 = request.form.get('Name')
    WMN1 = request.form.get('MN1')
    WMN2 = request.form.get('MN2')
    WMN3 = request.form.get('MN3')
    WMN4 = request.form.get('MN4')
    WMN5 = request.form.get('MN5')
    WMN6 = request.form.get('MN6')
    WMN7 = request.form.get('MN7')
    WMN8 = request.form.get('MN8')
    WMN9 = request.form.get('MN9')
    WMN10 = request.form.get('MN10')
    WB1 = request.form.get('B1')
    WB2 = request.form.get('B2')
    WB3 = request.form.get('B3')
    WB4 = request.form.get('B4')
    WB5 = request.form.get('B5')
    WB6 = request.form.get('B6')
    WB7 = request.form.get('B7')
    WB8 = request.form.get('B8')
    WB9 = request.form.get('B9')
    WB10 = request.form.get('B10')

    if(ToggleNumber == "1"):
        UART_functions.transmitbinary(WB1)
    elif(ToggleNumber == "2"):
        UART_functions.transmitbinary(WB2)
    elif(ToggleNumber == "3"):
        UART_functions.transmitbinary(WB3)
    elif(ToggleNumber == "4"):
        UART_functions.transmitbinary(WB4)
    elif(ToggleNumber == "5"):
        UART_functions.transmitbinary(WB5)
    elif(ToggleNumber == "6"):
        UART_functions.transmitbinary(WB6)
    elif(ToggleNumber == "7"):
        UART_functions.transmitbinary(WB7)
    elif(ToggleNumber == "8"):
        UART_functions.transmitbinary(WB8)
    elif(ToggleNumber == "9"):
        UART_functions.transmitbinary(WB9)
    elif(ToggleNumber == "10"):
        UART_functions.transmitbinary(WB10)


    return redirect("http://" + IP + "/?page=remote" + "&Loadselect" + "&Modeselect=" + Modeselect + "&Name=" + Name1 + "&MN1=" + WMN1 + "&MN2=" + WMN2 + "&MN3=" + WMN3 + "&MN4=" + WMN4 + "&MN5=" + WMN5 + "&MN6=" + WMN6 + "&MN7=" + WMN7 + "&MN8=" + WMN8 + "&MN9=" + WMN9 + "&MN10=" + WMN10 + "&B1=" + WB1 + "&B2=" + WB2 + "&B3=" + WB3 + "&B4=" + WB4 + "&B5=" + WB5 + "&B6=" + WB6 + "&B7=" + WB7 + "&B8=" + WB8 + "&B9=" + WB9 + "&B10=" + WB10)

@app.route("/SavePassword",methods=['GET', 'POST'])
def SavePassword():
    if RemoteServerTest == "True":
        IP = RemoteServerIP
    else:
        IP = request.args['IP']

    OldPass = request.form.get('OldPass')
    NewPass = request.form.get('NewPass')
    ConPass = request.form.get('ConPass')

    passw = Settings.query.filter_by(settingname = "password").first()
    passworddb = passw.settingvalue

    global loggedinclientip
    loggedinclientip1 = str(request.environ.get('HTTP_X_REAL_IP', request.remote_addr))
    if RemoteServerTest == "True":
        IP = RemoteServerIP
    else:
        IP = request.args['IP']

    if(loggedinclientip == loggedinclientip1 and loggedin == "true"):
        print("Logged In")
        if(OldPass == passworddb):
            print("old pass = password database")
            if(NewPass == ConPass):
                print("passwords match")
                account = Settings.query.filter_by(settingname = "password").first()
                account.settingvalue = NewPass
                db.session.commit()
                return redirect("http://" + IP + "/?page=settings")
            else:
                return redirect("http://" + IP + "/?page=settings&matched=false")
        else:
            return redirect("http://" + IP + "/?page=settings&matched=false")
    else:
        return redirect("http://" + IP + "/?page=logout2")

    return "error"

@app.route("/SaveIFTTT",methods=['GET','POST'])
def SaveIFTTT():
    if RemoteServerTest == "True":
        IP = RemoteServerIP
    else:
        IP = request.args['IP']

    loggedinclientip1 = str(request.environ.get('HTTP_X_REAL_IP', request.remote_addr))
    if(loggedinclientip == loggedinclientip1 and loggedin == "true"):
        SecretKey = request.form.get('SecretKey')
        Enabled = request.form.get("IFTTENABLED")

        IFTTTSecretKey = Settings.query.filter_by(settingname = "IFTTTSecretKey").first()
        IFTTTSecretKey.settingvalue = SecretKey
        db.session.commit()

        if(Enabled == "True"):
            IFTTTEnabled = Settings.query.filter_by(settingname = "IFTTTEnabled").first()
            IFTTTEnabled.settingvalue = "True"
            db.session.commit()
            return redirect("http://" + IP + "/?page=settings&SecretKey=" + IFTTTSecretKey)
        else:
            IFTTTEnabled = Settings.query.filter_by(settingname = "IFTTTEnabled").first()
            IFTTTEnabled.settingvalue = "False"
            db.session.commit()
            return redirect("http://" + IP + "/?page=settings&SecretKey=" + IFTTTSecretKey)

    return "error"

@app.route("/loadSettings", methods=['GET','POST'])
def loadSettings():
    if RemoteServerTest == "True":
        IP = RemoteServerIP
    else:
        IP = request.args['IP']

    loggedinclientip1 = str(request.environ.get('HTTP_X_REAL_IP', request.remote_addr))
    if(loggedinclientip == loggedinclientip1 and loggedin == "true"):
        IFTTTSecret = Settings.query.filter_by(settingname = "IFTTTSecretKey").first()
        IFTTTSecretKey= IFTTTSecret.settingvalue
        IFTTTEn = Settings.query.filter_by(settingname = "IFTTTEnabled").first()
        IFTENABLED = IFTTTEn.settingvalue
        return redirect("http://" + IP + "/?page=settings&SecretKey=" + IFTTTSecretKey + "&Enabled=" + IFTENABLED)

    return "Auth Error"

#other functions
def getTimeDifferenceFromNow(TimeStart1, TimeEnd1):
     timeDiff = TimeEnd1 - TimeStart1
     return timeDiff.total_seconds() / 60

if __name__ == "__main__":

    if RemoteServerTest == "True":
        print("ON REMOTE TEST")
    app.run(host='0.0.0.0',debug=True)
